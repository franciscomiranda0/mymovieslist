import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/shared/repository/storage_repository.dart';
import 'package:my_movies_list/shared/strings.dart';

abstract class Repository {
  String getUriOf(List<dynamic> addresses) {
    return addresses.fold(
      Strings.movieApiUrl,
      (previousValue, current) => '$previousValue/$current',
    );
  }

  Future<Map<String, String>> get authenticatedHeader async => {
        'Authorization':
            'Bearer ${await getIt.get<StorageRepository>().token ?? ''}',
        'Content-Type': 'application/json',
      };
}
