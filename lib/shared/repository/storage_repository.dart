import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/user/repository/models/user_model.dart';
import 'package:my_movies_list/shared/repository/repository.dart';

class StorageRepository extends Repository {
  static const _token = 'auth_token';
  final HttpService _httpService;
  final FlutterSecureStorage _secureStorage;
  UserModel? _user;

  StorageRepository(this._httpService)
      : _secureStorage = const FlutterSecureStorage();

  Future<String?> get token => _secureStorage.read(key: _token);

  Future<UserModel> get user async {
    if (_user == null) {
      final response = await _httpService.get(
        getUriOf(['auth', 'me']),
        header: await authenticatedHeader,
      );
      _user = UserModel.fromMap(response.data!);
    }
    return _user!;
  }

  Future<void> setToken(String token) async =>
      await _secureStorage.write(key: _token, value: token);

  Future<void> removeToken() async => await _secureStorage.delete(key: _token);
}
