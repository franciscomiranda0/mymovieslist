import 'package:flutter/material.dart';

class AsyncButton extends StatelessWidget {
  final bool isOnAsyncCall;
  final VoidCallback onTap;
  final Widget child;

  const AsyncButton({
    Key? key,
    required this.child,
    this.isOnAsyncCall = false,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: onTap,
      child: isOnAsyncCall ? const CircularProgressIndicator() : child,
    );
  }
}
