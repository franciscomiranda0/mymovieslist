import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

part 'poster_card_tag.dart';

class PosterCard extends StatelessWidget {
  final BoxFit fit;
  final String? label;
  final String posterUrl;

  const PosterCard({
    Key? key,
    required this.posterUrl,
    this.fit = BoxFit.contain,
    this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: const EdgeInsets.symmetric(
        horizontal: 8,
        vertical: 6,
      ),
      color: Colors.white,
      elevation: 12,
      shadowColor: Colors.black,
      child: Container(
        margin: const EdgeInsets.all(4),
        child: Column(
          children: [
            Image.network(
              posterUrl,
              fit: fit,
              height: 156,
              errorBuilder: (_, __, ___) {
                return const Text('Não foi possível carregar a imagem');
              },
            ),
            if (label != null) ...<Widget>[
              const SizedBox(height: 4),
              Text(
                _getFittedLabel(label!),
                style: const TextStyle(
                  fontSize: 11,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ]
          ],
        ),
      ),
    );
  }

  String _getFittedLabel(String label) {
    return label.length > 21 ? '${label.substring(0, 18)}..' : label;
  }
}
