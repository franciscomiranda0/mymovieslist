import 'package:flutter/material.dart';
import 'package:my_movies_list/shared/widgets/carousel.dart';

class LabeledCarousel extends StatelessWidget {
  final String label;
  final List<Widget> children;

  const LabeledCarousel({
    Key? key,
    required this.label,
    required this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            label,
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: 20.0,
              fontWeight: FontWeight.w700,
            ),
          ),
        ),
        const SizedBox(
          height: 8,
        ),
        Carousel(children: children),
      ],
    );
  }
}
