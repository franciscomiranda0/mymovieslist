import 'package:flutter/material.dart';

class HeadingText extends StatelessWidget {
  final String heading;
  final double fontSize;

  const HeadingText(
    this.heading, {
    Key? key,
    required this.fontSize,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(
      heading,
      style: TextStyle(
        color: Theme.of(context).primaryColor,
        fontSize: fontSize,
        fontWeight: FontWeight.bold,
      ),
      textAlign: TextAlign.left,
    );
  }
}
