import 'package:flutter/material.dart';

class Carousel extends StatelessWidget {
  final List<Widget> children;

  const Carousel({
    Key? key,
    required this.children,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: children.map((child) {
          return Padding(
            padding: const EdgeInsets.only(right: 8),
            child: child,
          );
        }).toList(),
      ),
    );
  }
}
