import 'package:flutter/material.dart';

class MmlTextFormField extends StatelessWidget {
  final TextEditingController controller;
  final Icon? icon;
  final TextInputType keyboardType;
  final String label;
  final bool obscureText;

  const MmlTextFormField({
    Key? key,
    required this.controller,
    this.icon,
    this.keyboardType = TextInputType.text,
    required this.label,
    this.obscureText = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        labelText: label,
        prefixIcon: icon,
      ),
      keyboardType: keyboardType,
      obscureText: obscureText,
    );
  }
}
