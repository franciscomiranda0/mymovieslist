part of '../../../shared/widgets/poster_card.dart';

class TitleCardTag extends StatelessWidget {
  final String content;

  const TitleCardTag(
    this.content, {
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      height: 20.0,
      margin: const EdgeInsets.symmetric(
        horizontal: 4,
      ),
      padding: const EdgeInsets.symmetric(
        horizontal: 4,
      ),
      child: Row(
        children: [
          Text(
            content,
            style: TextStyle(
              color: Theme.of(context).colorScheme.secondary,
              fontSize: 11.0,
            ),
          ),
        ],
      ),
    );
  }
}
