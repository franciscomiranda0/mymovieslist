import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class MmlSnackbar {
  static show({
    required BuildContext context,
    required String message,
    IconData? iconData,
  }) =>
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        behavior: SnackBarBehavior.floating,
        content: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            if (iconData != null) Icon(iconData),
            const SizedBox(width: 24),
            Text(message),
          ],
        ),
      ));
}
