import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/login/bloc/login_cubit.dart';
import 'package:my_movies_list/home/page/home_page.dart';
import 'package:my_movies_list/login/page/login_page.dart';

class SplashPage extends StatelessWidget {
  static const name = 'splash-page';

  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => LoginCubit()..hasLoggedUser(),
      child: const SplashView(),
    );
  }
}

class SplashView extends StatelessWidget {
  const SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginCubit, LoginState>(
      listener: (context, state) {
        if (state is LoginLoadFailure) _onLoginLoadFailure(context);
        if (state is LoginLoadSuccess) _onLoginLoadSuccess(context);
      },
      child: const Scaffold(
        body: Center(
          child: Icon(
            Icons.movie,
            size: 64,
          ),
        ),
      ),
    );
  }

  void _onLoginLoadFailure(BuildContext context) =>
      Navigator.pushReplacementNamed(context, LoginPage.name);

  void _onLoginLoadSuccess(BuildContext context) =>
      Navigator.pushReplacementNamed(context, HomePage.name);
}
