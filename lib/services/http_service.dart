import 'dart:convert';

import 'package:http/http.dart' as http;

class Response {
  final Map<String, dynamic>? data;
  final Exception? exception;
  final int statusCode;

  Response({
    this.data,
    this.exception,
    required this.statusCode,
  });

  bool get isSuccessful => statusCode >= 200 && statusCode < 300;
}

class HttpService {
  final _client = http.Client();

  Future<Response> post(
    String uri, {
    required Map<String, dynamic> body,
    Map<String, String>? header,
  }) async {
    final httpResponse = await _client.post(
      Uri.parse(uri),
      body: jsonEncode(body),
      headers: header ?? {'Content-Type': 'application/json'},
    );
    return _parseHttpResponse(httpResponse);
  }

  Future<Response> put(
    String uri,
    Map<String, dynamic> body, {
    Map<String, String>? header,
  }) async {
    final httpResponse = await _client.put(
      Uri.parse(uri),
      body: jsonEncode(body),
      headers: header,
    );
    return _parseHttpResponse(httpResponse);
  }

  Future<Response> get(
    String uri, {
    Map<String, dynamic>? params,
    Map<String, String>? header,
  }) async {
    if (params != null) uri += _getQueryString(params);

    final response = await _client.get(
      Uri.parse(uri),
      headers: header,
    );
    return _parseHttpResponse(response);
  }

  Future<Response> delete(
    String uri, {
    Map<String, String>? header,
  }) async {
    var httpResponse = await _client.delete(
      Uri.parse(uri),
      headers: header,
    );
    return _parseHttpResponse(httpResponse);
  }

  Response _parseHttpResponse(http.Response httpResponse) {
    late final Response response;
    try {
      response = Response(
        statusCode: httpResponse.statusCode,
        data: jsonDecode(httpResponse.body),
      );
    } on Exception catch (e) {
      response = Response(
        statusCode: 500,
        exception: e,
      );
    }
    return response;
  }

  String _getQueryString(Map<String, dynamic> params) {
    var query = '?';
    params.forEach((key, value) => query += '$key=$value&');
    return query;
  }
}
