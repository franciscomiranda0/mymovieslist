import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay_type_enum.dart';
import 'package:my_movies_list/home/bloc/main_tab_cubit.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';
import 'package:my_movies_list/screenplay/repository/movie_repository.dart';
import 'package:my_movies_list/screenplay/repository/screenplay_repository_interface.dart';
import 'package:my_movies_list/screenplay/repository/tv_show_repository.dart';
import 'package:my_movies_list/details/page/details_page.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/shared/widgets/labeled_carousel.dart';
import 'package:my_movies_list/shared/widgets/poster_card.dart';

class MainTabPage extends StatelessWidget {
  const MainTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => MainTabCubit(HttpService())..getScreenplays(),
      child: MainTabView(),
    );
  }
}

class MainTabView extends StatelessWidget {
  late final Set<_CarouselModel> _carousels;

  MainTabView({Key? key}) : super(key: key) {
    final _httpService = HttpService();
    _carousels = {
      _CarouselModel(
        label: 'Thriller',
        screenplayRepository: MovieRepository(_httpService),
        params: {'genre': 53},
      ),
      _CarouselModel(
        label: 'Histórico',
        screenplayRepository: TvShowRepository(_httpService),
        params: {'genre': 10768},
      ),
    };
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<MainTabCubit, MainTabState>(
      builder: (context, state) {
        return SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: 12),
              if (state is MainTabLoadSuccess)
                ...state.carousels.map((carousel) {
                  return Column(
                    children: [
                      LabeledCarousel(
                        label: carousel.label,
                        children:
                            _buildScreenplayCards(context, carousel.screeplays),
                      ),
                      const SizedBox(height: 24),
                    ],
                  );
                }),
            ],
          ),
        );
      },
    );
  }

  List<Widget> _buildScreenplayCards(
      BuildContext context, List<Screenplay> screenplays) {
    return screenplays
        .map((title) => GestureDetector(
              onTap: () =>
                  _onTapScreenplayCard(context, title.id, title.isTvShow),
              child: PosterCard(
                label: title.name,
                posterUrl: title.posterUrl,
              ),
            ))
        .toList();
  }

  void _onTapScreenplayCard(BuildContext context, int id, bool isTvShow) =>
      Navigator.of(context).pushNamed(
        DetailsPage.name,
        arguments: {
          'id': id,
          'screenplayType':
              isTvShow ? ScreenplayType.tvShow : ScreenplayType.movie,
        },
      );
}

class _CarouselModel {
  final String label;
  final ScreenplayRepositoryInterface screenplayRepository;
  final Map<String, dynamic>? params;

  _CarouselModel({
    required this.label,
    required this.screenplayRepository,
    this.params,
  });
}
