import 'package:flutter/material.dart';

class DrawerItem extends StatelessWidget {
  final IconData iconData;
  final String label;
  final VoidCallback onTap;

  const DrawerItem({
    Key? key,
    required this.iconData,
    required this.label,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      leading: Icon(
        iconData,
        color: Theme.of(context).primaryColor,
      ),
      onTap: onTap,
      subtitle: const Divider(),
      title: Text(label),
      visualDensity: VisualDensity.compact,
    );
  }
}
