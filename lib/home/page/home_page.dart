import 'package:flutter/material.dart';
import 'package:my_movies_list/home/page/widgets/drawer_item.dart';
import 'package:my_movies_list/home/page/widgets/main_tab_page.dart';
import 'package:my_movies_list/home/page/widgets/movies_tab_page.dart';
import 'package:my_movies_list/home/page/widgets/series_tab_page.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/login/page/login_page.dart';
import 'package:my_movies_list/rating/page/user_ratings_page.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/shared/repository/storage_repository.dart';
import 'package:my_movies_list/search/page/search_page.dart';
import 'package:my_movies_list/user/page/users_page.dart';

class HomePage extends StatelessWidget {
  static const name = 'home-page';

  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: SafeArea(
        child: Scaffold(
          appBar: AppBar(
            actions: [
              IconButton(
                onPressed: () {
                  Navigator.of(context).pushNamed(SearchPage.name);
                },
                icon: const Icon(Icons.search),
              ),
            ],
            bottom: const TabBar(
              tabs: [
                Tab(child: Text('Principal')),
                Tab(child: Text('Filmes')),
                Tab(child: Text('Séries')),
              ],
            ),
            title: const Text('MyMoviesApp'),
          ),
          drawer: Drawer(
            child: Column(
              children: [
                DrawerItem(
                  iconData: Icons.account_circle_outlined,
                  label: 'usuários',
                  onTap: () {
                    Navigator.pushNamed(context, UsersPage.name);
                  },
                ),
                DrawerItem(
                  iconData: Icons.thumbs_up_down_outlined,
                  label: 'minhas classificações',
                  onTap: () async {
                    final user = await StorageRepository(HttpService()).user;
                    Navigator.of(context)
                        .pushNamed(UserRatingsPage.name, arguments: user.id);
                  },
                ),
                DrawerItem(
                  iconData: Icons.exit_to_app,
                  label: 'sair',
                  onTap: () async {
                    await getIt.get<StorageRepository>().removeToken();
                    Navigator.pushNamedAndRemoveUntil(
                        context, LoginPage.name, (context) => false);
                  },
                ),
              ],
            ),
          ),
          body: const TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children: [
              MainTabPage(),
              MoviesTabPage(),
              SeriesTabPage(),
            ],
          ),
        ),
      ),
    );
  }
}
