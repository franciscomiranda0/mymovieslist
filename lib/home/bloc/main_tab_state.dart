part of 'main_tab_cubit.dart';

@immutable
abstract class MainTabState {}

class MainTabLoadInProgress implements MainTabState {}

class MainTabLoadSuccess implements MainTabState {
  final List<CarouselModel> carousels;

  MainTabLoadSuccess(this.carousels);
}

class MainTabLoadError implements MainTabState {}
