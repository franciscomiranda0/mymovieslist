import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';
import 'package:my_movies_list/screenplay/repository/movie_repository.dart';
import 'package:my_movies_list/screenplay/repository/screenplay_repository_interface.dart';
import 'package:my_movies_list/screenplay/repository/tv_show_repository.dart';
import 'package:my_movies_list/services/http_service.dart';

part 'main_tab_state.dart';

class MainTabCubit extends Cubit<MainTabState> {
  final List<CarouselRequest> carousels;
  late ScreenplayRepositoryInterface _screenplayRepository;

  MainTabCubit(HttpService _httpService)
      : carousels = [
          CarouselRequest(
            label: 'Thriller',
            repository: MovieRepository(_httpService),
            params: {'genre': 53},
          ),
          CarouselRequest(
            label: 'Animação',
            repository: MovieRepository(_httpService),
            params: {'genre': 16},
          ),
          CarouselRequest(
            label: 'Histórico',
            repository: TvShowRepository(_httpService),
            params: {'genre': 10768},
          ),
        ],
        super(MainTabLoadInProgress());

  Future<void> getScreenplays() async {
    final _carousels = <CarouselModel>[];
    try {
      for (final request in carousels) {
        _screenplayRepository = request.repository;
        final screenplays =
            await _screenplayRepository.getScreenplays(params: request.params);
        _carousels.add(CarouselModel(request.label, screenplays));
      }
      emit(MainTabLoadSuccess(_carousels));
    } catch (e) {
      emit(MainTabLoadError());
    }
  }
}

class CarouselRequest {
  final String label;
  final ScreenplayRepositoryInterface repository;
  final Map<String, dynamic> params;

  CarouselRequest({
    required this.label,
    required this.repository,
    required this.params,
  });
}

class CarouselModel {
  final String label;
  final List<Screenplay> screeplays;

  CarouselModel(this.label, this.screeplays);
  // final Map<String, List<Screenplay>> entries;

  // CarouselModel() : entries = {};
}
