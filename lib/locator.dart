import 'package:get_it/get_it.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/shared/repository/storage_repository.dart';
import 'package:my_movies_list/user/repository/user_repository.dart';
import 'package:my_movies_list/user/repository/user_repository_interface.dart';

var getIt = GetIt.instance;

void setUpLocator() {
  getIt.registerLazySingleton(() => HttpService());
  getIt.registerLazySingleton<StorageRepository>(
      () => StorageRepository(getIt.get<HttpService>()));
  getIt.registerLazySingleton<UserRepositoryInterface>(
      () => UserRepositoryImpl(getIt.get<HttpService>()));
}
