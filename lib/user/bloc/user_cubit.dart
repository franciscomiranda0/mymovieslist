import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';
import 'package:my_movies_list/user/repository/models/user_model.dart';
import 'package:my_movies_list/user/repository/user_repository_interface.dart';

part 'user_state.dart';

class UserCubit extends Cubit<UserState> {
  final UserRepositoryInterface _userRepository;

  UserCubit(this._userRepository) : super(UserInitial());

  Future<void> getUserRatings(String id) async {
    emit(UserLoadInProgress());
    try {
      final ratings = await _userRepository.getUserRatings(id);
      emit(UserRatingsLoadSuccess(ratings));
    } catch (e) {
      emit(UserLoadError(e.toString()));
    }
  }

  Future<void> getUsers() async {
    emit(UserLoadInProgress());
    try {
      final users = await _userRepository.getUsers();
      emit(UsersLoadSuccess(users));
    } catch (e) {
      emit(UserLoadError(e.toString()));
    }
  }
}
