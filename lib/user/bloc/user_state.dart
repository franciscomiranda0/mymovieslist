part of 'user_cubit.dart';

@immutable
abstract class UserState {}

class UserInitial implements UserState {}

class UserLoadInProgress implements UserState {}

class UsersLoadSuccess implements UserState {
  final List<UserModel> users;

  UsersLoadSuccess(this.users);
}

class UserRatingsLoadSuccess implements UserState {
  final List<Screenplay> screenplays;

  UserRatingsLoadSuccess(this.screenplays);
}

class UserLoadError implements UserState {
  final String message;

  UserLoadError(this.message);
}
