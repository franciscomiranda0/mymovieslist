import 'package:flutter/material.dart';

class UserDetails extends StatelessWidget {
  final String name;
  final VoidCallback? onTap;

  const UserDetails({
    Key? key,
    required this.name,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: [
          CircleAvatar(
            child: Text(
              _getInitials(name),
            ),
            backgroundColor: Theme.of(context).primaryColor,
            foregroundColor: Theme.of(context).secondaryHeaderColor,
          ),
          const SizedBox(width: 8),
          Text(
            name,
            style: TextStyle(
              color: Theme.of(context).primaryColor,
              fontSize: Theme.of(context).textTheme.subtitle1?.fontSize,
              fontWeight: Theme.of(context).textTheme.subtitle1?.fontWeight,
            ),
          ),
        ],
      ),
    );
  }

  String _getInitials(String fullName) {
    final _names = fullName.split(' ');
    final _initials = <String>[];
    for (var name in _names) {
      _initials.add(name.substring(0, 1));
    }
    return _initials.join();
  }
}
