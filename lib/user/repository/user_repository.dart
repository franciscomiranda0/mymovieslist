import 'package:my_movies_list/shared/repository/repository.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/user/repository/models/user_model.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';
import 'package:my_movies_list/user/repository/user_repository_interface.dart';

class UserRepositoryImpl extends Repository implements UserRepositoryInterface {
  final HttpService _httpService;

  UserRepositoryImpl(this._httpService);

  @override
  Future<List<Screenplay>> getUserRatings(String id) async {
    final response = await _httpService.get(
      getUriOf(['users', id, 'titles-rated']),
      header: await authenticatedHeader,
    );
    return _mapResponseToModels<Screenplay>(
        response.data!['data'], (map) => Screenplay.fromMap(map));
  }

  @override
  Future<List<UserModel>> getUsers() async {
    final response = await _httpService.get(
      getUriOf(['users']),
      header: await authenticatedHeader,
    );
    return _mapResponseToModels<UserModel>(
        response.data!['data'], (map) => UserModel.fromMap(map));
  }

  List<T> _mapResponseToModels<T extends Mapped>(
    List data,
    T Function(Map<String, dynamic>) fromMap,
  ) {
    return List<T>.from(data.map((map) => fromMap(map)));
  }
}
