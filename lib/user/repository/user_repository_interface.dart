import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';
import 'package:my_movies_list/user/repository/models/user_model.dart';

abstract class UserRepositoryInterface {
  Future<List<UserModel>> getUsers();
  Future<List<Screenplay>> getUserRatings(String id);
}
