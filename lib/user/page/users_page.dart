import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/rating/page/user_ratings_page.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/user/bloc/user_cubit.dart';
import 'package:my_movies_list/user/repository/models/user_details.dart';
import 'package:my_movies_list/user/repository/user_repository.dart';

class UsersPage extends StatelessWidget {
  static const name = 'users-page';
  const UsersPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserCubit(
        UserRepositoryImpl(HttpService()),
      )..getUsers(),
      child: const UsersView(),
    );
  }
}

class UsersView extends StatelessWidget {
  const UsersView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.all(8),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 12),
                Text(
                  'usuários',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: Theme.of(context).textTheme.headline5?.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.headline5?.fontWeight,
                  ),
                ),
                const SizedBox(height: 24),
                BlocBuilder<UserCubit, UserState>(
                  builder: (context, state) {
                    if (state is UsersLoadSuccess) {
                      return ListView.builder(
                        physics: const ScrollPhysics(),
                        shrinkWrap: true,
                        itemCount: state.users.length,
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              UserDetails(
                                name: state.users[index].name,
                                onTap: () =>
                                    _onUserTap(context, state.users[index].id),
                              ),
                              const SizedBox(height: 8),
                            ],
                          );
                        },
                      );
                    } else {
                      return const SizedBox();
                    }
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onUserTap(BuildContext context, String id) async {
    Navigator.of(context).pushNamed(UserRatingsPage.name, arguments: id);
  }
}
