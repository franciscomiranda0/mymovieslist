part of 'screenplay_cubit.dart';

@immutable
abstract class ScreenplayState {}

class ScreenplayInitial implements ScreenplayState {}

class ScreenplayLoadInProgress implements ScreenplayState {}

class ScreenplayListingLoadSuccess implements ScreenplayState {
  final List<Screenplay> screenplays;

  ScreenplayListingLoadSuccess(this.screenplays);
}

class ScreenplayLoadSuccess implements ScreenplayState {
  final Screenplay screenplay;

  ScreenplayLoadSuccess(this.screenplay);
}

class ScreenplayLoadFailure implements ScreenplayState {}

class ScreenplayLoadError implements ScreenplayState {}
