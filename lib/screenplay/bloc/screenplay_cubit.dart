import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';
import 'package:my_movies_list/screenplay/repository/screenplay_repository_interface.dart';

part 'screenplay_state.dart';

class ScreenplayCubit extends Cubit<ScreenplayState> {
  ScreenplayRepositoryInterface _repository;

  ScreenplayCubit(this._repository) : super(ScreenplayInitial());

  set repository(ScreenplayRepositoryInterface repository) =>
      _repository = repository;

  Future<void> getScreenplays({Map<String, dynamic>? params}) async {
    emit(ScreenplayLoadInProgress());
    try {
      final screenplays = await _repository.getScreenplays(params: params);
      screenplays.isNotEmpty
          ? emit(ScreenplayListingLoadSuccess(screenplays))
          : emit(ScreenplayLoadFailure());
    } catch (e) {
      emit(ScreenplayLoadError());
    }
  }
}
