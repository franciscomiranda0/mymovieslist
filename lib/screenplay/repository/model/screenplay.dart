import 'package:my_movies_list/user/repository/models/user_model.dart';

class Screenplay extends Mapped<Screenplay> {
  final int id;
  final String name;
  final String coverUrl;
  final String posterUrl;
  final bool isTvShow;
  final int rating;

  Screenplay._({
    required this.id,
    required this.name,
    required this.coverUrl,
    required this.posterUrl,
    required this.isTvShow,
    required this.rating,
  });

  factory Screenplay.fromMap(Map map) {
    return Screenplay._(
      id: map['id'],
      name: map['name'],
      coverUrl: map['cover_url'],
      posterUrl: map['poster_url'],
      isTvShow: map['is_tv_show'],
      rating: map['rate'] ?? 0,
    );
  }

  factory Screenplay.fromRatingMap(Map map) {
    return Screenplay._(
      id: map['id'],
      name: map['name'],
      coverUrl: map['cover_url'],
      posterUrl: map['poster_url'],
      isTvShow: false,
      rating: map['rate'],
    );
  }

  @override
  String toString() {
    return '''
      id: $id,
      name: $name,
      posterUrl: $posterUrl,
      coverUrl: $coverUrl,
      isTvShow: $isTvShow,
      rating: $rating,
    ''';
  }
}
