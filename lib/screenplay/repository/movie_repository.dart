import 'package:my_movies_list/shared/repository/repository.dart';
import 'package:my_movies_list/details/repository/models/details_model.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';
import 'package:my_movies_list/screenplay/repository/screenplay_repository_interface.dart';
import 'package:my_movies_list/services/http_service.dart';

class MovieRepository extends Repository
    implements ScreenplayRepositoryInterface {
  final HttpService _httpService;

  MovieRepository(this._httpService);

  @override
  Future<DetailsModel> getDetails(int id) async {
    final response = await _httpService.get(getUriOf(['movies', id]));
    return DetailsModel.fromMap(response.data!);
  }

  @override
  Future<int> getRating(int id) async {
    final response = await _httpService.get(
      getUriOf(['movies', id, 'rate']),
      header: await authenticatedHeader,
    );
    return response.data!['rate'];
  }

  @override
  Future<List<Screenplay>> getRecommendations(int id) async {
    final response =
        await _httpService.get(getUriOf(['movies', id, 'recommendations']));
    return response.isSuccessful
        ? _mapResponseToModels(response.data!['data'])
        : [];
  }

  List<Screenplay> _mapResponseToModels(List data) {
    return List<Screenplay>.from(data.map((map) => Screenplay.fromMap(map)));
  }

  @override
  Future<List<Screenplay>> getScreenplays({
    Map<String, dynamic>? params,
  }) async {
    final response = await _httpService.get(
      getUriOf(['movies']),
      params: params,
    );
    return response.isSuccessful
        ? _mapResponseToModels(response.data!['data'])
        : [];
  }

  @override
  Future<bool> removeComment(int screenplayId, commentId) async {
    final response = await _httpService.delete(
      getUriOf(['movies', screenplayId, commentId, 'comment']),
      header: await authenticatedHeader,
    );
    return response.isSuccessful;
  }

  @override
  Future<bool> saveComment(int screenplayId, String comment) async {
    final response = await _httpService.post(
      getUriOf(['movies', screenplayId, 'comment']),
      header: await authenticatedHeader,
      body: {'comment': comment},
    );
    return response.isSuccessful;
  }

  @override
  Future<bool> saveRating(int screenplayId, int rating) async {
    final response = await _httpService.post(
      getUriOf(['movies', screenplayId, 'rate']),
      header: await authenticatedHeader,
      body: {'rate': rating},
    );
    return response.isSuccessful;
  }
}
