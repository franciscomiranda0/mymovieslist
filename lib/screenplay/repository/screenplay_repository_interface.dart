import 'package:my_movies_list/details/repository/models/details_model.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';

abstract class ScreenplayRepositoryInterface {
  Future<DetailsModel> getDetails(int id);
  Future<int> getRating(int id);
  Future<List<Screenplay>> getRecommendations(int id);
  Future<List<Screenplay>> getScreenplays({Map<String, dynamic>? params});
  Future<bool> removeComment(int screenplayId, commentId);
  Future<bool> saveComment(int screenplayId, String comment);
  Future<bool> saveRating(int screenplayId, int rating);
}
