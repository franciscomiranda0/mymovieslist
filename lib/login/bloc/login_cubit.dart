import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/shared/repository/repository.dart';
import 'package:my_movies_list/login/repository/exceptions/user_not_found_exception.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/shared/repository/storage_repository.dart';

part 'login_state.dart';
part '../repository/login_repository_interface.dart';
part '../repository/login_repository.dart';

class LoginCubit extends Cubit<LoginState> {
  final LoginRepository _loginRepository;

  LoginCubit()
      : _loginRepository = LoginRepositoryImpl(
          httpService: HttpService(),
          storageRepository: StorageRepository(HttpService()),
        ),
        super(LoginInitial());

  Future<void> login(LoginRequest loginRequest) async {
    emit(LoginLoadInProgress());
    try {
      await _loginRepository.login(loginRequest);
      emit(LoginLoadSuccess());
    } on UserNotFoundException catch (e) {
      emit(LoginLoadError(e.message));
    } catch (e) {
      emit(LoginLoadError(e.toString()));
    }
  }

  Future<void> hasLoggedUser() async {
    await _loginRepository.hasLoggedUser
        ? emit(LoginLoadSuccess())
        : emit(LoginLoadFailure());
  }
}
