part of 'login_cubit.dart';

@immutable
abstract class LoginState {}

class LoginInitial implements LoginState {}

class LoginLoadInProgress implements LoginState {}

class LoginLoadSuccess implements LoginState {}

class LoginLoadFailure implements LoginState {}

class LoginLoadError implements LoginState {
  final String message;

  LoginLoadError(this.message);
}
