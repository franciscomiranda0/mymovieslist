class EmailAlreadyInUseException implements Exception {
  final String message;

  EmailAlreadyInUseException(this.message);
}
