part of '../bloc/login_cubit.dart';

class LoginRepositoryImpl extends Repository implements LoginRepository {
  final HttpService httpService;
  final StorageRepository storageRepository;

  LoginRepositoryImpl({
    required this.httpService,
    required this.storageRepository,
  });

  @override
  Future<bool> get hasLoggedUser async => await storageRepository.token != null;

  @override
  Future<void> login(LoginRequest request) async {
    final response = await httpService.post(
      getUriOf(['auth', 'login']),
      body: {
        'email': request.email,
        'password': request.password,
      },
    );
    if (response.isSuccessful) {
      await storageRepository.setToken(response.data!['token']);
    } else if (response.statusCode == 400) {
      throw UserNotFoundException('Usuário não encontrado');
    } else {
      throw Exception('Falha ao realizar login');
    }
  }
}
