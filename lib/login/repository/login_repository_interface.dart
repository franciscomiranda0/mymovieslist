part of '../bloc/login_cubit.dart';

abstract class LoginRepository {
  Future<void> login(LoginRequest request);
  Future<bool> get hasLoggedUser;
}

class LoginRequest {
  final String email;
  final String password;

  LoginRequest({
    required this.email,
    required this.password,
  });
}
