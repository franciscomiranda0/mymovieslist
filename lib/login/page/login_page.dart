import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/login/bloc/login_cubit.dart';
import 'package:my_movies_list/shared/mml_snackbar.dart';
import 'package:my_movies_list/shared/widgets/mml_text_form_field.dart';
import 'package:my_movies_list/home/page/home_page.dart';
import 'package:my_movies_list/signup/page/signup_page.dart';
import 'package:my_movies_list/shared/widgets/async_button.dart';
import 'package:my_movies_list/shared/widgets/heading_text.dart';

class LoginPage extends StatelessWidget {
  static const name = 'login-page';
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => LoginCubit(),
      child: LoginView(),
    );
  }
}

class LoginView extends StatelessWidget {
  final TextEditingController _emailController;
  final TextEditingController _passwordController;

  LoginView({Key? key})
      : _emailController = TextEditingController(),
        _passwordController = TextEditingController(),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginCubit, LoginState>(
      listener: (context, state) {
        if (state is LoginLoadError) _onLoginLoadError(context, state.message);
        if (state is LoginLoadSuccess) _onLoginLoadSucces(context);
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 148),
                HeadingText(
                  'login',
                  fontSize: Theme.of(context).textTheme.headline5!.fontSize!,
                ),
                const SizedBox(height: 24),
                MmlTextFormField(
                  controller: _emailController,
                  icon: const Icon(Icons.email_outlined),
                  keyboardType: TextInputType.emailAddress,
                  label: 'email',
                ),
                const SizedBox(height: 16),
                MmlTextFormField(
                  controller: _passwordController,
                  icon: const Icon(Icons.password_outlined),
                  label: 'senha',
                  obscureText: true,
                ),
                const SizedBox(height: 16),
                BlocBuilder<LoginCubit, LoginState>(
                  builder: (context, state) {
                    return AsyncButton(
                      isOnAsyncCall: state is LoginLoadInProgress,
                      onTap: () {
                        context.read<LoginCubit>().login(
                              LoginRequest(
                                email: _emailController.text,
                                password: _passwordController.text,
                              ),
                            );
                      },
                      child: const Text('entrar'),
                    );
                  },
                ),
                const SizedBox(height: 16),
                const Text(
                  'ou',
                  textAlign: TextAlign.center,
                ),
                TextButton(
                  onPressed: () => _onTapCreateAccount(context),
                  child: const Text('criar uma conta'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onLoginLoadError(BuildContext context, String message) {
    MmlSnackbar.show(context: context, message: message);
  }

  void _onLoginLoadSucces(BuildContext context) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(HomePage.name, (route) => false);
  }

  void _onTapCreateAccount(BuildContext context) =>
      Navigator.of(context).pushNamed(SignUpPage.name);
}
