import 'package:flutter/material.dart';

class RatingRadioButton extends StatefulWidget {
  final void Function(int)? onChanged;
  final int value;

  const RatingRadioButton({
    Key? key,
    this.onChanged,
    required this.value,
  }) : super(key: key);

  @override
  _RatingRadioButtonState createState() => _RatingRadioButtonState();
}

class _RatingRadioButtonState extends State<RatingRadioButton> {
  static const _thumbDown = 'thumb_down';
  static const _thumbUp = 'thumb_up';
  final Map<String, bool> _selection;

  _RatingRadioButtonState()
      : _selection = {
          _thumbDown: false,
          _thumbUp: false,
        };

  @override
  Widget build(BuildContext context) {
    _selection.updateAll((key, value) =>
        (widget.value == -1 && key == _thumbDown) ||
        (widget.value == 1 && key == _thumbUp));
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          onPressed: () {
            _toggleSelection(_thumbDown);
            widget.onChanged?.call(-1);
          },
          icon: Icon(
            _isSelected(_thumbDown)
                ? Icons.thumb_down
                : Icons.thumb_down_outlined,
          ),
        ),
        IconButton(
          onPressed: () {
            _toggleSelection(_thumbUp);
            widget.onChanged?.call(1);
          },
          icon: Icon(
            _isSelected(_thumbUp) ? Icons.thumb_up : Icons.thumb_up_outlined,
          ),
        ),
      ],
    );
  }

  void _toggleSelection(String selected) {
    setState(() => _selection.updateAll((key, value) => key == selected));
  }

  bool _isSelected(String option) => _selection[option]!;
}
