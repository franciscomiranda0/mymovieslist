import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/rating/page/widgets/rating_radio_button.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/user/bloc/user_cubit.dart';
import 'package:my_movies_list/user/repository/user_repository.dart';

class UserRatingsPage extends StatelessWidget {
  static const name = 'user-rating-page';
  const UserRatingsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final id = ModalRoute.of(context)!.settings.arguments as String;
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => UserCubit(
            UserRepositoryImpl(HttpService()),
          )..getUserRatings(id),
        ),
      ],
      child: const UserRatingsView(),
    );
  }
}

class UserRatingsView extends StatelessWidget {
  const UserRatingsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 12),
                Text(
                  'classificações',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: Theme.of(context).textTheme.headline5!.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.headline5!.fontWeight,
                  ),
                ),
                const SizedBox(height: 24),
                BlocBuilder<UserCubit, UserState>(
                  builder: (context, state) {
                    if (state is UserRatingsLoadSuccess) {
                      return GridView.builder(
                        padding: const EdgeInsets.only(bottom: 24),
                        shrinkWrap: true,
                        gridDelegate:
                            const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2,
                          crossAxisSpacing: 12,
                          mainAxisSpacing: 12,
                        ),
                        itemCount: state.screenplays.length,
                        itemBuilder: (context, index) {
                          Image cover =
                              Image.network(state.screenplays[index].coverUrl);
                          return Card(
                            elevation: 8,
                            shape: const RoundedRectangleBorder(
                                borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(16),
                              topRight: Radius.circular(16),
                            )),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: const BorderRadius.only(
                                  topLeft: Radius.circular(16),
                                  topRight: Radius.circular(16),
                                ),
                                image: DecorationImage(
                                  alignment: Alignment.topCenter,
                                  image: cover.image,
                                ),
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    state.screenplays[index].name,
                                    style: TextStyle(
                                      color: Theme.of(context).primaryColor,
                                    ),
                                  ),
                                  // const SizedBox(height: 8),
                                  RatingRadioButton(
                                    value: state.screenplays[index].rating,
                                  ),
                                ],
                              ),
                            ),
                          );
                        },
                      );
                    }
                    return const SizedBox();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
