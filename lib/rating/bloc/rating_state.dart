part of 'rating_cubit.dart';

@immutable
abstract class RatingState {
  final int rating;

  const RatingState(this.rating);
}

class RatingInitial extends RatingState {
  const RatingInitial() : super(0);
}

class RatingLoadInProgress extends RatingState {
  const RatingLoadInProgress(int rating) : super(rating);
}

class RatingLoadSuccess extends RatingState {
  const RatingLoadSuccess(int rating) : super(rating);
}

class RatingLoadError extends RatingState {
  final String message;

  const RatingLoadError(this.message, int rating) : super(rating);
}

class RatingSaveInProgress extends RatingState {
  const RatingSaveInProgress(int rating) : super(rating);
}

class RatingSaveSuccess extends RatingState {
  const RatingSaveSuccess(int rating) : super(rating);
}

class RatingSaveFailure extends RatingState {
  const RatingSaveFailure(int rating) : super(rating);
}

class RatingSaveError extends RatingState {
  final String message;

  const RatingSaveError(this.message, int rating) : super(rating);
}
