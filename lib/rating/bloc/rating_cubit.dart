import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/screenplay/repository/screenplay_repository_interface.dart';

part 'rating_state.dart';

class RatingCubit extends Cubit<RatingState> {
  ScreenplayRepositoryInterface _screenplayRepository;

  RatingCubit(this._screenplayRepository) : super(RatingInitial());

  Future<void> getRating(int id) async {
    emit(RatingLoadInProgress(state.rating));
    try {
      final rating = await _screenplayRepository.getRating(id);
      emit(RatingLoadSuccess(rating));
    } catch (e) {
      emit(RatingLoadError(e.toString(), state.rating));
    }
  }

  Future<void> saveRating(int id, int rating) async {
    emit(RatingSaveInProgress(state.rating));
    try {
      await _screenplayRepository.saveRating(id, rating)
          ? emit(RatingLoadSuccess(rating))
          : emit(RatingSaveFailure(state.rating));
    } catch (e) {
      emit(RatingSaveError(e.toString(), state.rating));
    }
  }

  set screenplayRepository(ScreenplayRepositoryInterface repository) =>
      _screenplayRepository = repository;
}
