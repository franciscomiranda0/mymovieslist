import 'package:flutter/material.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/details/page/details_page.dart';
import 'package:my_movies_list/home/page/home_page.dart';
import 'package:my_movies_list/login/page/login_page.dart';
import 'package:my_movies_list/rating/page/user_ratings_page.dart';
import 'package:my_movies_list/signup/page/signup_page.dart';
import 'package:my_movies_list/search/page/search_page.dart';
import 'package:my_movies_list/splash/page/splash_page.dart';
import 'package:my_movies_list/user/page/users_page.dart';

void main() {
  setUpLocator();
  runApp(const MyMoviesList());
}

class MyMoviesList extends StatelessWidget {
  const MyMoviesList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.from(
        colorScheme: ColorScheme.light(
          primary: Colors.red.shade900,
          secondary: Colors.red.shade50,
        ),
      ),
      debugShowCheckedModeBanner: false,
      title: 'My Movies List',
      initialRoute: SplashPage.name,
      routes: {
        LoginPage.name: (_) => const LoginPage(),
        HomePage.name: (_) => const HomePage(),
        SearchPage.name: (_) => const SearchPage(),
        DetailsPage.name: (_) => DetailsPage(),
        SignUpPage.name: (_) => const SignUpPage(),
        UserRatingsPage.name: (_) => const UserRatingsPage(),
        UsersPage.name: (_) => const UsersPage(),
        SplashPage.name: (_) => const SplashPage(),
      },
    );
  }
}
