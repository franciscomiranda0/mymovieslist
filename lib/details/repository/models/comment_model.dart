class CommentModel {
  int id;
  String text;
  DateTime date;

  CommentModel._({
    required this.id,
    required this.text,
    required this.date,
  });

  factory CommentModel.fromJson(Map<String, dynamic> json) {
    return CommentModel._(
      id: json['id'],
      text: json['text'] ?? '',
      date: DateTime.parse(json['date']),
    );
  }
}
