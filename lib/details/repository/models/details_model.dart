import 'package:my_movies_list/details/repository/models/comment_model.dart';

class DetailsModel {
  final int id;
  final String name;
  final String overview;
  final DateTime releaseDate;
  final num voteAverage;
  final String coverUrl;
  final String runtime;
  final List<String> genres;
  final List<CommentModel> comments;

  DetailsModel._({
    required this.id,
    required this.name,
    required this.overview,
    required this.releaseDate,
    required this.voteAverage,
    required this.coverUrl,
    required this.runtime,
    required this.genres,
    required this.comments,
  });

  DetailsModel.initial({
    this.id = 0,
    this.name = '',
    this.overview = '',
    this.voteAverage = 0,
    this.coverUrl = '',
    this.runtime = '',
    this.genres = const [],
    this.comments = const [],
  }) : releaseDate = DateTime.now();

  factory DetailsModel.fromMap(Map<String, dynamic> map) {
    return DetailsModel._(
      id: map['id'],
      name: map['name'],
      overview: map['overview'],
      releaseDate: DateTime.parse(map['release_date']),
      voteAverage: map['vote_average'],
      coverUrl: map['cover_url'],
      runtime: map['runtime'],
      genres: _getGenres(map['genres']),
      comments: _getComments(map['comments']),
    );
  }

  static List<CommentModel> _getComments(List commentMaps) {
    final comments = <CommentModel>[];
    for (var item in commentMaps) {
      comments.add(CommentModel.fromJson(item));
    }
    return comments;
  }

  static List<String> _getGenres(List genresMaps) {
    final genres = <String>[];
    for (var map in genresMaps) {
      genres.add(map['name']);
    }
    return genres;
  }

  @override
  String toString() {
    return '''
      id: $id,
      name: $name,
      overview: $overview,
      releaseDate: $releaseDate,
      voteAverage: $voteAverage,
      coverUrl: $coverUrl,
      runtime: $runtime,
      genres: $genres,
    ''';
  }
}
