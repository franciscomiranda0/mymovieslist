import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';
import 'package:my_movies_list/screenplay/repository/screenplay_repository_interface.dart';

part 'recommendations_state.dart';

class RecommendationsCubit extends Cubit<RecommendationsState> {
  final ScreenplayRepositoryInterface _screenplayRepository;

  RecommendationsCubit(this._screenplayRepository)
      : super(RecommendationsInitial());

  Future<void> getRecommendations(int id) async {
    emit(RecommendationsLoadInProgress());
    try {
      final screenplays = await _screenplayRepository.getRecommendations(id);
      emit(RecommendationsLoadSuccess(screenplays));
    } catch (e) {
      emit(RecommendationsLoadError(e.toString()));
    }
  }
}
