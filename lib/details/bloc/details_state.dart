part of 'details_cubit.dart';

@immutable
abstract class DetailsState {
  final DetailsModel details;

  const DetailsState(this.details);
}

class DetailsInitial extends DetailsState {
  const DetailsInitial(DetailsModel details) : super(details);
}

class DetailsLoadInProgress extends DetailsState {
  const DetailsLoadInProgress(DetailsModel details) : super(details);
}

class DetailsLoadSuccess extends DetailsState {
  const DetailsLoadSuccess(DetailsModel details) : super(details);
}

class DetailsCommentSaveSuccess extends DetailsState {
  const DetailsCommentSaveSuccess(DetailsModel details) : super(details);
}

class DetailsCommentRemoveSuccess extends DetailsState {
  const DetailsCommentRemoveSuccess(DetailsModel details) : super(details);
}

class DetailsError extends DetailsState {
  final String message;

  const DetailsError(
    this.message,
    DetailsModel details,
  ) : super(details);
}
