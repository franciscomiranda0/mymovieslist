part of 'recommendations_cubit.dart';

@immutable
abstract class RecommendationsState {}

class RecommendationsInitial implements RecommendationsState {}

class RecommendationsLoadInProgress implements RecommendationsState {}

class RecommendationsLoadSuccess implements RecommendationsState {
  final List<Screenplay> screenplays;

  RecommendationsLoadSuccess(this.screenplays);
}

class RecommendationsLoadError implements RecommendationsState {
  final String message;

  RecommendationsLoadError(this.message);
}
