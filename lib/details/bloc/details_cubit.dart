import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/details/repository/models/details_model.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/screenplay/repository/screenplay_repository_interface.dart';
import 'package:my_movies_list/shared/repository/storage_repository.dart';
import 'package:my_movies_list/user/repository/models/user_model.dart';

part 'details_state.dart';

class DetailsCubit extends Cubit<DetailsState> {
  final ScreenplayRepositoryInterface _screenplayRepository;

  DetailsCubit(this._screenplayRepository)
      : super(DetailsInitial(DetailsModel.initial()));

  Future<void> getDetails(int id) async {
    emit(DetailsLoadInProgress(state.details));
    try {
      final details = await _screenplayRepository.getDetails(id);
      emit(DetailsLoadSuccess(details));
    } catch (e) {
      emit(DetailsError(e.toString(), state.details));
    }
  }

  Future<void> saveComment(int id, String comment) async {
    emit(DetailsLoadInProgress(state.details));
    try {
      if (await _screenplayRepository.saveComment(id, comment)) {
        final details = await _screenplayRepository.getDetails(id);
        emit(DetailsCommentSaveSuccess(details));
      }
    } catch (e) {
      emit(DetailsError(e.toString(), state.details));
    }
  }

  Future<void> deleteComment(int id, int commentId) async {
    emit(DetailsLoadInProgress(state.details));
    try {
      final UserModel user = await getIt.get<StorageRepository>().user;
      if (id.toString() == user.id) return;
      if (await _screenplayRepository.removeComment(id, commentId)) {
        final details = await _screenplayRepository.getDetails(id);
        emit(DetailsCommentRemoveSuccess(details));
      }
    } catch (e) {
      emit(DetailsError(e.toString(), state.details));
    }
  }
}
