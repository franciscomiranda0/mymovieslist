import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay_type_enum.dart';
import 'package:my_movies_list/details/bloc/details_cubit.dart';
import 'package:my_movies_list/details/bloc/recommendations_cubit.dart';
import 'package:my_movies_list/details/page/widgets/comment_section.dart';
import 'package:my_movies_list/details/page/widgets/details_label_content.dart';
import 'package:my_movies_list/rating/bloc/rating_cubit.dart';
import 'package:my_movies_list/rating/page/widgets/rating_radio_button.dart';
import 'package:my_movies_list/screenplay/repository/movie_repository.dart';
import 'package:my_movies_list/screenplay/repository/screenplay_repository_interface.dart';
import 'package:my_movies_list/screenplay/repository/tv_show_repository.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/shared/widgets/carousel.dart';
import 'package:my_movies_list/shared/widgets/heading_text.dart';

class DetailsProvider {
  final int id;
  final ScreenplayType screenplayType;

  DetailsProvider({
    required this.id,
    required this.screenplayType,
  });
}

class DetailsPage extends StatelessWidget {
  static const name = 'details-page';
  final HttpService _httpService;
  late final int _id;
  late final ScreenplayType _screenplayType;
  late final ScreenplayRepositoryInterface _repository;

  DetailsPage({Key? key})
      : _httpService = HttpService(),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    final args = (ModalRoute.of(context)!.settings.arguments as Map);
    _id = args['id'];
    _screenplayType = args['screenplayType'];
    _repository = (_screenplayType == ScreenplayType.movie
        ? MovieRepository(_httpService)
        : TvShowRepository(_httpService)) as ScreenplayRepositoryInterface;
    return RepositoryProvider<DetailsProvider>(
      create: (context) {
        return DetailsProvider(
          id: _id,
          screenplayType: _screenplayType,
        );
      },
      child: MultiBlocProvider(
        providers: [
          BlocProvider(
            create: (context) => DetailsCubit(_repository)..getDetails(_id),
          ),
          BlocProvider(
            create: (context) =>
                RecommendationsCubit(_repository)..getRecommendations(_id),
          ),
          BlocProvider(
            create: (context) => RatingCubit(_repository)..getRating(_id),
          ),
        ],
        child: const DetailsView(),
      ),
    );
  }
}

class DetailsView extends StatelessWidget {
  const DetailsView({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: GestureDetector(
          onTap: () => _unfocus(context),
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                BlocBuilder<DetailsCubit, DetailsState>(
                  builder: (context, state) {
                    return Column(
                      children: [
                        Center(
                          child: Image.network(
                            state.details.coverUrl,
                            errorBuilder: (_, __, ___) {
                              return const Text('Falha ao carregar imagem');
                            },
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              HeadingText(
                                state.details.name,
                                fontSize: 22,
                              ),
                              const SizedBox(height: 8.0),
                              DetailsLabelContent(
                                label: 'Sinopse:',
                                content: state.details.overview,
                              ),
                              const SizedBox(height: 8.0),
                              DetailsLabelContent(
                                label: 'Duração:',
                                content: '${state.details.runtime}min',
                              ),
                              Carousel(
                                children: state.details.genres.map((genre) {
                                  return Chip(label: Text(genre));
                                }).toList(),
                              ),
                            ],
                          ),
                        ),
                      ],
                    );
                  },
                ),
                BlocBuilder<RecommendationsCubit, RecommendationsState>(
                  builder: (context, state) {
                    if (state is RecommendationsLoadSuccess) {
                      return Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Carousel(
                          children: state.screenplays.map((screenplay) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.of(context).pushReplacementNamed(
                                  DetailsPage.name,
                                  arguments: {
                                    'id': screenplay.id,
                                    'screenplayType': screenplay.isTvShow
                                        ? ScreenplayType.tvShow
                                        : ScreenplayType.movie,
                                  },
                                );
                              },
                              // _onCoverTap(context, title.id, title.isTvShow),
                              child: Image.network(
                                screenplay.coverUrl,
                                height: 96,
                              ),
                            );
                          }).toList(),
                        ),
                      );
                    } else {
                      return const SizedBox();
                    }
                  },
                ),
                const SizedBox(height: 4),
                BlocBuilder<RatingCubit, RatingState>(
                  builder: (context, state) {
                    return RatingRadioButton(
                      value: state.rating,
                      onChanged: (int rating) {
                        context.read<RatingCubit>().saveRating(
                            context.read<DetailsProvider>().id, rating);
                      },
                    );
                  },
                ),
                BlocBuilder<DetailsCubit, DetailsState>(
                  builder: (context, state) {
                    return Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: CommentSection(
                        comments: state.details.comments,
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _unfocus(BuildContext context) => FocusScope.of(context).unfocus();
}
