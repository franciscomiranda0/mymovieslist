import 'package:flutter/material.dart';

class Comment extends StatelessWidget {
  final String content;
  final void Function() onTap;

  const Comment(
    this.content, {
    Key? key,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              content,
              style: TextStyle(
                fontSize: Theme.of(context).textTheme.bodyText2?.fontSize,
                fontWeight: Theme.of(context).textTheme.bodyText2?.fontWeight,
              ),
            ),
            IconButton(
              onPressed: onTap,
              icon: const Icon(Icons.delete_outline),
            ),
          ],
        ),
        const Divider(),
      ],
    );
  }
}
