import 'package:flutter/material.dart';
import 'package:my_movies_list/details/bloc/details_cubit.dart';
import 'package:my_movies_list/details/page/details_page.dart';
import 'package:my_movies_list/details/page/widgets/comment.dart';
import 'package:my_movies_list/details/repository/models/comment_model.dart';
import 'package:my_movies_list/shared/widgets/mml_text_form_field.dart';
import 'package:my_movies_list/shared/widgets/async_button.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CommentSection extends StatelessWidget {
  final TextEditingController _commentController;
  final List<CommentModel> comments;

  CommentSection({
    Key? key,
    required this.comments,
  })  : _commentController = TextEditingController(),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(
          crossAxisAlignment: CrossAxisAlignment.end,
          mainAxisSize: MainAxisSize.min,
          children: [
            Flexible(
              child: MmlTextFormField(
                controller: _commentController,
                label: 'adicionar comentário',
              ),
            ),
            const SizedBox(width: 8),
            AsyncButton(
              child: const Text('ok'),
              onTap: () => _onTapOk(context, _commentController.text),
            ),
            const SizedBox(width: 8),
          ],
        ),
        ListView.builder(
          physics: const ScrollPhysics(),
          shrinkWrap: true,
          itemCount: comments.length,
          itemBuilder: (context, index) {
            return Comment(
              comments[index].text,
              onTap: () => _onTapDelete(context, comments[index].id),
            );
          },
        ),
      ],
    );
  }

  void _onTapOk(BuildContext context, String comment) {
    context
        .read<DetailsCubit>()
        .saveComment(context.read<DetailsProvider>().id, comment);
  }

  void _onTapDelete(BuildContext context, int commentId) {
    context
        .read<DetailsCubit>()
        .deleteComment(context.read<DetailsProvider>().id, commentId);
  }
}
