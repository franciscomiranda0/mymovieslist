import 'package:flutter/material.dart';

class DetailsLabelContent extends StatelessWidget {
  final String label;
  final String content;

  const DetailsLabelContent({
    Key? key,
    required this.label,
    required this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          label,
          style: TextStyle(
            fontSize: Theme.of(context).textTheme.bodyText1!.fontSize,
            fontWeight: Theme.of(context).textTheme.bodyText1!.fontWeight,
          ),
        ),
        const SizedBox(width: 8),
        Expanded(
          flex: 6,
          child: Text(
            content,
            textAlign: TextAlign.justify,
            style: TextStyle(
              fontSize: Theme.of(context).textTheme.bodyText2!.fontSize,
              fontWeight: Theme.of(context).textTheme.bodyText2!.fontWeight,
            ),
          ),
        ),
      ],
    );
  }
}
