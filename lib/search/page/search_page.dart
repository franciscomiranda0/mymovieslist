import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/search/bloc/search_bloc.dart';
import 'package:my_movies_list/search/repository/search_repository.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/shared/widgets/poster_card.dart';

class SearchPage extends StatelessWidget {
  static const name = 'search-page';

  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => SearchBloc(
        SearchRepository(HttpService()),
      ),
      child: SearchView(),
    );
  }
}

class SearchView extends StatelessWidget {
  final TextEditingController _queryController;
  final ScrollController _scrollController;

  SearchView({Key? key})
      : _queryController = TextEditingController(),
        _scrollController = ScrollController(),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextFormField(
          controller: _queryController,
          onChanged: (query) =>
              context.read<SearchBloc>().add(SearchQueryTyped(query)),
          decoration: const InputDecoration(
            border: InputBorder.none,
            hintText: 'pesquise aqui..',
            hintStyle: TextStyle(
              color: Colors.white38,
            ),
          ),
        ),
      ),
      body: BlocBuilder<SearchBloc, SearchState>(
        builder: (context, state) {
          return NotificationListener(
            onNotification: (notification) {
              if (notification is ScrollEndNotification) {
                if (notification.metrics.pixels >=
                    notification.metrics.maxScrollExtent * .8) {
                  context.read<SearchBloc>().add(SearchScrollEnded());
                }
              }
              return true;
            },
            child: ListView.builder(
              controller: _scrollController,
              shrinkWrap: true,
              itemCount: state.screenplays.length,
              itemBuilder: (context, index) {
                return PosterCard(
                  posterUrl: state.screenplays[index].posterUrl,
                  fit: BoxFit.fill,
                );
              },
            ),
          );
        },
      ),
    );
  }
}
