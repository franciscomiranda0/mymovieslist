import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';

class SearchResponse {
  final List<Screenplay> movies;
  final int totalMovies;

  SearchResponse({
    required this.movies,
    required this.totalMovies,
  });

  factory SearchResponse.fromMap(Map<String, dynamic> map) {
    return SearchResponse(
      movies: List<Screenplay>.from(
          map['data'].map((map) => Screenplay.fromMap(map['data']))),
      totalMovies: map['count'],
    );
  }
}
