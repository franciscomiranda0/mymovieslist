import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';
import 'package:my_movies_list/search/repository/models/search_response.dart';

abstract class SearchRepositoryInterface {
  Future<SearchResponse> getQuery({required Map<String, dynamic> params});
}
