import 'package:my_movies_list/shared/repository/repository.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';
import 'package:my_movies_list/search/repository/models/search_response.dart';
import 'package:my_movies_list/search/repository/search_repository_interface.dart';
import 'package:my_movies_list/services/http_service.dart';

class SearchRepository extends Repository implements SearchRepositoryInterface {
  final HttpService _httpService;

  SearchRepository(this._httpService);

  @override
  Future<SearchResponse> getQuery(
      {required Map<String, dynamic> params}) async {
    final moviesResponse = await _httpService.get(
      getUriOf(['movies']),
      header: await authenticatedHeader,
      params: params,
    );

    return _mapResponseToModels(moviesResponse.data!);
  }

  SearchResponse _mapResponseToModels(Map<String, dynamic> map) {
    return SearchResponse(
      movies: List<Screenplay>.from(
          map['data'].map((map) => Screenplay.fromMap(map))),
      totalMovies: map['count'],
    );
  }
}
