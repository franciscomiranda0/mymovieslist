part of 'search_bloc.dart';

enum SearchStateType {
  searchInitial,
  searchLoadInProgress,
  searchLoadSuccess,
  searchLoadError,
}

class SearchState {
  final bool hasReachedEnd;
  final _Page page;
  final String query;
  final List<Screenplay> screenplays;
  final SearchStateType type;

  SearchState._({
    required this.hasReachedEnd,
    required this.page,
    required this.query,
    required this.screenplays,
    required this.type,
  });

  SearchState.initial({
    this.hasReachedEnd = false,
    this.page = const _Page.initial(),
    this.query = '',
    this.screenplays = const [],
    this.type = SearchStateType.searchInitial,
  });

  SearchState copyWith({
    bool? hasReachedEnd,
    _Page? page,
    String? query,
    List<Screenplay>? screenplays,
    required SearchStateType type,
  }) {
    return SearchState._(
      hasReachedEnd: hasReachedEnd ?? this.hasReachedEnd,
      page: page ?? this.page,
      query: query ?? this.query,
      screenplays: screenplays ?? this.screenplays,
      type: type,
    );
  }
}

class _Page {
  static const amountEach = 10;
  final int current;
  final int total;

  _Page._({
    required this.current,
    required this.total,
  });

  const _Page.initial({
    this.current = 1,
    this.total = 0,
  });

  _Page copyWith({
    int? current,
    int? total,
  }) {
    return _Page._(
      current: current ?? this.current,
      total: total ?? this.total,
    );
  }
}
