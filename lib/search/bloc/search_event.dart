part of 'search_bloc.dart';

abstract class SearchEvent {
  final bool searchForMovies;

  SearchEvent({this.searchForMovies = true});
}

class SearchQueryTyped extends SearchEvent {
  final String query;

  SearchQueryTyped(
    this.query, {
    bool searchForMovies = true,
  }) : super(searchForMovies: searchForMovies);
}

class SearchScrollEnded extends SearchEvent {
  SearchScrollEnded({
    bool searchForMovies = true,
  }) : super(searchForMovies: searchForMovies);
}
