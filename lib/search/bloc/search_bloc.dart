import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/screenplay/repository/model/screenplay.dart';
import 'package:my_movies_list/search/repository/search_repository_interface.dart';
import 'package:rxdart/rxdart.dart';

part 'search_event.dart';
part 'search_state.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  final SearchRepositoryInterface _searchRepository;

  SearchBloc(
    this._searchRepository,
  ) : super(SearchState.initial()) {
    on<SearchQueryTyped>(
      (event, emit) async {
        try {
          emit(state.copyWith(
            query: event.query,
            type: SearchStateType.searchLoadInProgress,
          ));
          final search = await _searchRepository.getQuery(
            params: {
              'name': event.query,
              'page': state.page.current,
            },
          );
          final totalPages =
              _getTotalPages(search.totalMovies, _Page.amountEach);
          emit(state.copyWith(
            hasReachedEnd: _hasReachedEnd(state.page.current, totalPages),
            page: state.page.copyWith(
              total: totalPages,
            ),
            query: event.query,
            screenplays: search.movies,
            type: SearchStateType.searchLoadSuccess,
          ));
        } catch (e) {
          emit(state.copyWith(
            type: SearchStateType.searchLoadError,
          ));
        }
      },
      transformer: debounce(const Duration(seconds: 1)),
    );

    on<SearchScrollEnded>((event, emit) async {
      if (state.hasReachedEnd) return;
      emit(state.copyWith(
        type: SearchStateType.searchLoadInProgress,
      ));
      try {
        final search = await _searchRepository.getQuery(
          params: {
            'name': state.query,
            'page': state.page.current + 1,
          },
        );
        emit(state.copyWith(
          hasReachedEnd:
              _hasReachedEnd(state.page.current + 1, state.page.total),
          page: state.page.copyWith(
            current: state.page.current + 1,
          ),
          query: state.query,
          screenplays: List.from(state.screenplays + search.movies),
          type: SearchStateType.searchLoadSuccess,
        ));
      } catch (e) {
        emit(state.copyWith(
          type: SearchStateType.searchLoadError,
        ));
      }
    });
  }

  EventTransformer<T> debounce<T>(Duration duration) {
    return (events, mapper) => events.debounceTime(duration).flatMap(mapper);
  }

  int _getTotalPages(int totalEntries, int amountEachPage) {
    return (totalEntries / amountEachPage).ceil();
  }

  bool _hasReachedEnd(int currentPage, int totalPages) =>
      currentPage == totalPages;
}
