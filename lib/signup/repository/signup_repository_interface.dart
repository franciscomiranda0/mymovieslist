part of '../bloc/signup_cubit.dart';

abstract class SignUpRepository {
  Future<void> signUp(SignUpRequest signUpRequest);
}

class SignUpRequest {
  final String name;
  final String email;
  final String password;

  SignUpRequest({
    required this.name,
    required this.email,
    required this.password,
  });
}
