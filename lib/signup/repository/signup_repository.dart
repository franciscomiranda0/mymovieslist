part of '../bloc/signup_cubit.dart';

class SignUpRepositoryImpl extends Repository implements SignUpRepository {
  final HttpService httpService;
  final StorageRepository storageRepository;

  SignUpRepositoryImpl({
    required this.httpService,
    required this.storageRepository,
  });

  @override
  Future<void> signUp(SignUpRequest signUpRequest) async {
    final response = await httpService.post(
      getUriOf(['auth', 'register']),
      body: {
        'name': signUpRequest.name,
        'email': signUpRequest.email,
        'password': signUpRequest.password,
      },
    );
    if (response.isSuccessful) {
      await storageRepository.setToken(response.data!['token']);
    } else if (response.statusCode == 400) {
      throw EmailAlreadyInUseException('Email já cadastrado');
    } else {
      throw Exception('Falha na requisição');
    }
  }
}
