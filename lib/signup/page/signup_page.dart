import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/shared/mml_snackbar.dart';
import 'package:my_movies_list/shared/repository/storage_repository.dart';
import 'package:my_movies_list/shared/widgets/mml_text_form_field.dart';
import 'package:my_movies_list/signup/bloc/signup_cubit.dart';
import 'package:my_movies_list/login/page/login_page.dart';
import 'package:my_movies_list/shared/widgets/async_button.dart';

class SignUpPage extends StatelessWidget {
  static const name = 'signup-page';

  const SignUpPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) {
        final httpService = HttpService();
        return SignUpCubit(
          signUpRepository: SignUpRepositoryImpl(
            httpService: httpService,
            storageRepository: StorageRepository(httpService),
          ),
        );
      },
    );
  }
}

class SignUpView extends StatelessWidget {
  final TextEditingController _nameController;
  final TextEditingController _emailController;
  final TextEditingController _passwordController;

  SignUpView({Key? key})
      : _nameController = TextEditingController(),
        _emailController = TextEditingController(),
        _passwordController = TextEditingController(),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SignUpCubit, SignUpState>(
      listener: (context, state) {
        if (state is SignUpLoadError)
          _onSignUpLoadError(context, state.message);
        if (state is SignUpLoadSuccess) _onSignUpLoadSuccess(context);
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                const SizedBox(height: 148),
                Text(
                  'cadastro',
                  style: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: Theme.of(context).textTheme.headline5?.fontSize,
                    fontWeight:
                        Theme.of(context).textTheme.headline5?.fontWeight,
                  ),
                ),
                const SizedBox(height: 24),
                MmlTextFormField(
                  controller: _nameController,
                  label: 'nome',
                  icon: const Icon(Icons.person),
                ),
                const SizedBox(height: 16),
                MmlTextFormField(
                  controller: _emailController,
                  icon: const Icon(Icons.email_outlined),
                  label: 'email',
                  keyboardType: TextInputType.emailAddress,
                ),
                const SizedBox(height: 16),
                MmlTextFormField(
                  controller: _passwordController,
                  icon: const Icon(Icons.password_outlined),
                  label: 'senha',
                  obscureText: true,
                ),
                const SizedBox(height: 16),
                BlocBuilder<SignUpCubit, SignUpState>(
                  builder: (context, state) {
                    return AsyncButton(
                      isOnAsyncCall: state is SignUpLoadInProgress,
                      onTap: () => context.read<SignUpCubit>().signUp(
                            SignUpRequest(
                              name: _nameController.text,
                              email: _emailController.text,
                              password: _passwordController.text,
                            ),
                          ),
                      child: const Text('confirmar'),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _onSignUpLoadError(BuildContext context, String message) =>
      MmlSnackbar.show(context: context, message: message);

  void _onSignUpLoadSuccess(BuildContext context) =>
      Navigator.pushReplacementNamed(context, LoginPage.name);
}
