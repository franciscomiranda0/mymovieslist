part of 'signup_cubit.dart';

@immutable
abstract class SignUpState {}

class SignUpInitial implements SignUpState {}

class SignUpLoadInProgress implements SignUpState {}

class SignUpLoadSuccess implements SignUpState {}

class SignUpLoadError implements SignUpState {
  final String message;

  SignUpLoadError(this.message);
}
