import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/shared/repository/repository.dart';
import 'package:my_movies_list/login/repository/exceptions/email_already_in_use_exception.dart';
import 'package:my_movies_list/services/http_service.dart';
import 'package:my_movies_list/shared/repository/storage_repository.dart';

part 'signup_state.dart';
part '../repository/signup_repository_interface.dart';
part '../repository/signup_repository.dart';

class SignUpCubit extends Cubit<SignUpState> {
  final SignUpRepository signUpRepository;

  SignUpCubit({required this.signUpRepository}) : super(SignUpInitial());

  Future<void> signUp(SignUpRequest signUpRequest) async {
    emit(SignUpLoadInProgress());
    try {
      await signUpRepository.signUp(signUpRequest);
      emit(SignUpLoadSuccess());
    } on EmailAlreadyInUseException catch (e) {
      emit(SignUpLoadError(e.message));
    } catch (e) {
      emit(SignUpLoadError(e.toString()));
    }
  }
}
